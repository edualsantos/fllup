'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PostSchema = new Schema({
  type: String, //What if...Could have...wouldn't will be better if...It'll be awesome if... etc
  text: String,
  src: String,
  src_type: {type:String, default:'image'}, //video, link or image

  user_following: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  comments: [{type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}],

  interactions: {
    total_upvote: { type: Number, default: 0 },
    total_downvote: {type: Number, default: 0 },
    total_comments: {type: Number, default: 0 },
    total_user_following: {type: Number, default: 0 }
  },

  createdBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  createdAt: {type: Date, default: Date.now },
  updatedAt: {type: Date, default: Date.now },
  lastActivity: {type: Date, default: Date.now },

  active: Boolean
});

module.exports = mongoose.model('Post', PostSchema);
