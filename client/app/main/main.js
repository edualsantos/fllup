'use strict';

angular.module('fllup')
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/ops");

    $stateProvider
      .state('main', {
        url: '',
        abstract: true,
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })

      .state('main.feed',{
        url:'/feed',
        templateUrl: '../app/feed/feed.html',
        controller: 'FeedCtrl'
      })

      .state('main.profile',{
        url:'/@:username',
        templateUrl: '../app/account/profile/profile.html',
        controller: 'ProfileCtrl'
      })

    /**
     * Page brand or product
     */
      .state('main.page',{
        url:'page/:pagename',
        templateUrl: '../app/account/profile/profile.html',
        controller: 'ProfileCtrl'
      })


      .state('main.login', {
        url: '/login',
        templateUrl: 'app/account/login/login.html',
        controller: 'LoginCtrl'
      })
      .state('main.companySignup', {
        url: '/company/signup',
        templateUrl: 'app/account/signup/companySignup.html',
        controller: 'SignupCtrl'
      })

      .state('main.userSignup', {
        url: '/user/signup',
        templateUrl: 'app/account/signup/userSignup.html',
        controller: 'SignupCtrl'
      })
      .state('main.settings', {
        url: '/settings',
        templateUrl: 'app/account/settings/settings.html',
        controller: 'SettingsCtrl',
        authenticate: true
      })
      .state('main.ops', { //404
        url: '/ops',
        templateUrl: 'components/404/404.html'
        //controller: 'SettingsCtrl',
        //authenticate: true
      });



  });
