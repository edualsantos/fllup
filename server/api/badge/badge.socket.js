/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Badge = require('./badge.model');

exports.register = function(socket) {
  Badge.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Badge.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('badge:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('badge:remove', doc);
}