'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var BadgeSchema = new Schema({
  name: String,
  info: String,
  badge_image: String,
  active: Boolean
});

module.exports = mongoose.model('Badge', BadgeSchema);
