'use strict';

angular.module('fllup', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'btford.socket-io',
  'ui.router',
  'pascalprecht.translate',
  'ngStorage'
])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('authInterceptor');
  })

  .factory('authInterceptor', function ($rootScope, $q, $cookieStore, $location) {
    return {
      // Add authorization token to headers
      request: function (config) {
        config.headers = config.headers || {};
        if ($cookieStore.get('token')) {
          config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
        }
        return config;
      },

      // Intercept 401s and redirect you to login
      responseError: function(response) {
        if(response.status === 401) {
          $location.path('/login');
          // remove any stale tokens
          $cookieStore.remove('token');
          return $q.reject(response);
        } else
        if(response.status === 404) {
          $location.path('/404');
          // remove any stale tokens
          //$cookieStore.remove('token');
          return $q.reject(response);
        }
        else {
          return $q.reject(response);
        }
      }
    };
  })

  .config(['$translateProvider', function($translateProvider){
    // Register a loader for the static files
    // So, the module will search missing translation tables under the specified urls.
    // Those urls are [prefix][langKey][suffix].
    $translateProvider.useStaticFilesLoader({
      prefix: 'app/l10n/',
      suffix: '.json'
    });
    // Tell the module what language to use by default
    $translateProvider.preferredLanguage('en');
    // Tell the module to store the language in the local storage
    $translateProvider.useLocalStorage();
  }])

  .run(function ($rootScope, $location,$translate, $window, Auth) {
    // add 'ie' classes to html
    var isIE = !!navigator.userAgent.match(/MSIE/i);
    isIE && angular.element($window.document.body).addClass('ie');
    isSmartDevice( $window ) && angular.element($window.document.body).addClass('smart');

    $rootScope.lang = { isopen: false };
    $rootScope.langs = {en:'English', de_DE:'German', it_IT:'Italian', pt_BR:'Português'};
    $rootScope.selectLang = $rootScope.langs[$translate.proposedLanguage()] || "English";
    $rootScope.setLang = function(langKey, $event) {
      // set the current lang
      $rootScope.selectLang = $rootScope.langs[langKey];
      console.log($rootScope.selectLang);
      // You can change the language during runtime
      $translate.use(langKey);
      $rootScope.lang.isopen = !$rootScope.lang.isopen;
    };

    function isSmartDevice( $window )
    {
      // Adapted from http://www.detectmobilebrowsers.com
      var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
      // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
      return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
    }

    $rootScope.app = {
      navbar : {
        visible : false
      },
      subheader : {
        visible : false
      },
      noHeaderVisible : false,
      noSubHeaderVisible : false,
      states : {
          name : ''
        }
      };

    function navVisible(value){
      $rootScope.app.noHeaderVisible = value;
      $rootScope.app.noSubHeaderVisibible = value;

      $rootScope.app.navbar.visible = value;
      $rootScope.app.subheader.visible = value;

    }

    $rootScope.$on('$stateChangeSuccess',
      function(event, toState, toParams, fromState, fromParams){
        $rootScope.app.states.name = toState.name;
        //console.log('outside',toState.name);
        //Quando nao for a pagina de login, mostra o navbar
        if ( ( toState.name.indexOf('login')  <= -1 )
          && ( toState.name.indexOf('Signup')  <= -1 )
          && ( toState.name.indexOf('settings')  <= -1 )
          && ( toState.name.indexOf('ops')  <= -1 ) // 404
        ) {
          console.log(toState.name);
          navVisible(true);
        }else{
          navVisible(false);
        }
      });



    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$stateChangeStart', function (event, next) {
      Auth.isLoggedInAsync(function(loggedIn) {
        if (next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  });
