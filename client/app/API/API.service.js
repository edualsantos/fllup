'use strict';

angular.module('fllup')
  .factory('API', function () {
    var promise = null;
    var service = {};

    /**
     * Get all posts based on page ( for pagination )
     * @param page
     * @returns {*}
     */
    service.getPosts = function(page){
      promise = $http.get('api/posts',{_page : page});
      return promise;
    };
    /**
     * Create a post
     * @param item
     * @returns {*}
     */
    service.setCreatePost = function(item){
      promise = $http.post('api/post',{_item : item});
      return promise;
    };
    /**
     * Get a list of TOP posts
     * @param item
     * @returns {*}
     */
    service.setTopPost = function(item){
      promise = $http.get('api/post/top',{_item : item});
      return promise;
    };





    /**
     * Get a specific comment by PostID
     * @param post_id
     * @returns {*}
     */
    service.getComments = function(post_id){
      promise = $http.get('api/comments',{_post_id : post_id});
      return promise;
    };
    /**
     * Get a specific post by PostID
     * @param post_id
     * @returns {*}
     */
    service.getComment = function(post_id){
      promise = $http.get('api/comment',{_post_id : post_id});
      return promise;
    };
    /**
     * Create a Comment at some Post
     * @param post
     * @returns {*}
     */
    service.setComment = function(post_id, comment){
      promise = $http.post('api/comment',{_post_id: post_id, _comment: comment});
      return promise;
    };





    /**
     * Like post or comment
     * @param id
     * @param at
     * @returns {*}
     */
    service.setLike = function(id, at){
      promise = $http.post('api/like',{_id : id, _at : at});
      return promise;
    };
    /**
     * Dislike fome post of comment
     * @param id
     * @param at
     * @returns {*}
     */
    service.setDislike = function(id, at){
      promise = $http.post('api/dislike',{_id : id, _at : at});
      return promise;
    };





    /**
     * Get a list of companys registere in the platform
     * @param page
     * @returns {*}
     */
    service.getCompanyList = function(page){
      promise = $http.get('api/company',{_page : page});
      return promise;
    };
    /**
     * Get a list of TOP companyies
     * @param page
     * @returns {*}
     */
    service.getTopCompanies = function(page){
      promise = $http.get('api/company/top',{_page : page});
      return promise;
    };

    service.getFollowListPost = function(page){
      promise = $http.get('api/company/following/posts',{_page : page});
      return promise;
    };


    // Public API here
    return service;
  });
