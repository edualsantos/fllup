'use strict';

var _ = require('lodash');
var Badge = require('./badge.model');

// Get list of badges
exports.index = function(req, res) {
  Badge.find(function (err, badges) {
    if(err) { return handleError(res, err); }
    return res.json(200, badges);
  });
};

// Get a single badge
exports.show = function(req, res) {
  Badge.findById(req.params.id, function (err, badge) {
    if(err) { return handleError(res, err); }
    if(!badge) { return res.send(404); }
    return res.json(badge);
  });
};

// Creates a new badge in the DB.
exports.create = function(req, res) {
  Badge.create(req.body, function(err, badge) {
    if(err) { return handleError(res, err); }
    return res.json(201, badge);
  });
};

// Updates an existing badge in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Badge.findById(req.params.id, function (err, badge) {
    if (err) { return handleError(res, err); }
    if(!badge) { return res.send(404); }
    var updated = _.merge(badge, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, badge);
    });
  });
};

// Deletes a badge from the DB.
exports.destroy = function(req, res) {
  Badge.findById(req.params.id, function (err, badge) {
    if(err) { return handleError(res, err); }
    if(!badge) { return res.send(404); }
    badge.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}