'use strict';

var _ = require('lodash');
var Hunt = require('./hunt.model');

// Get list of hunts
exports.index = function(req, res) {
  Hunt.find(function (err, hunts) {
    if(err) { return handleError(res, err); }
    return res.json(200, hunts);
  });
};

// Get a single hunt
exports.show = function(req, res) {
  Hunt.findById(req.params.id, function (err, hunt) {
    if(err) { return handleError(res, err); }
    if(!hunt) { return res.send(404); }
    return res.json(hunt);
  });
};

// Creates a new hunt in the DB.
exports.create = function(req, res) {
  Hunt.create(req.body, function(err, hunt) {
    if(err) { return handleError(res, err); }
    return res.json(201, hunt);
  });
};

// Updates an existing hunt in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Hunt.findById(req.params.id, function (err, hunt) {
    if (err) { return handleError(res, err); }
    if(!hunt) { return res.send(404); }
    var updated = _.merge(hunt, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, hunt);
    });
  });
};

// Deletes a hunt from the DB.
exports.destroy = function(req, res) {
  Hunt.findById(req.params.id, function (err, hunt) {
    if(err) { return handleError(res, err); }
    if(!hunt) { return res.send(404); }
    hunt.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}