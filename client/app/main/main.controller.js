'use strict';

angular.module('fllup')
  .controller('MainCtrl', function ($scope, $http, socket) {

    $scope.notifyO = function(){
      var options = {
        style : 'circle',
        message : 'You have receive a new message from the comapny that you Help = )',
        position : 'top-right',
        type : 'success',
        showClose : true,
        title : 'Title of this notification',
        thumbnail: "<img width='40' height='40' style='display: inline-block;' src='assets/img/profiles/3.jpg' data-src='assets/img/profiles/3.jpg' data-src-retina='assets/img/profiles/3.jpg' alt=''>"
      };
      $('body').pgNotification(options).show();
      console.log('notified');
    };

    $scope.awesomeThings = [];
    $scope.username="Meu User name";
    $scope.do_comment = false;
    $scope.posts = [
      {id : 1, src: '../../assets/img/profiles/1.jpg', username:'Eduardo Almeida', text:'Wish the play function worked on mobile. I like being able to see the comments very clearly. Cool project.', createAt:'10 minutes ago'},
      {id : 2, src: '../../assets/img/profiles/2.jpg', username:'John Cares', text:'Absolutely lovely interface focusing on content. And yes, although I agree on the fact that SoundCloud comments arent the best, its still good to see what people you share tastes with have to say.', createAt:'last week'},
      {id : 3, src: '../../assets/img/profiles/2.jpg', username:'Hugo chaves', text:'Haha, great name for a SoundCloud comment product! Sadly though, this doesnt work at all for me. Turned off AdBlock and enabled plugins in Chrome (both of which sometimes dont play nice).', createAt:'one hour ago'},
      {id : 4, src: '../../assets/img/profiles/3.jpg', username:'Leopoldo Vilela', text:'his makes me feel like I in a club. Really, really cool!!! Well done!!!', createAt:'2 minutes'},
      {id : 5, src: '../../assets/img/profiles/4.jpg', username:'Caroline Hugg', text:'Commenting is limited for now but will open to those recommended by the community.', createAt:'11 January'},
      {id : 6, src: '../../assets/img/profiles/5.jpg', username:'Bianca Riccio', text:'An ideia without a good execution is only an ideia', createAt:'8 days ago'},
      {id : 7, src: '../../assets/img/profiles/6.jpg', username:'Patricio Gruticci', text:'An ideia without a good execution is only an ideia', createAt:'just now'}
    ];


    //Toggle comment box
    $scope.commentToggle = function(active){
      $scope.do_comment = !active;
    };

    $scope.items = ['item1', 'item2', 'item3'];

    $scope.open = function (size) {
      $('#modalFillIn').modal('show');
      //$("#modalFillIn").css("z-index", "1500");
    };

    $('#modalFillIn').on('show.bs.modal', function(e) {
      $('body').addClass('fill-in-modal');
    });
    $('#modalFillIn').on('hidden.bs.modal', function(e) {
      $('body').removeClass('fill-in-modal');
    });

    $http.get('/api/things').success(function(awesomeThings) {
      $scope.awesomeThings = awesomeThings;
      socket.syncUpdates('thing', $scope.awesomeThings);
    });

    $scope.addThing = function() {
      if($scope.newThing === '') {
        return;
      }
      $http.post('/api/things', { name: $scope.newThing });
      $scope.newThing = '';
    };

    $scope.deleteThing = function(thing) {
      $http.delete('/api/things/' + thing._id);
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('thing');
    });
  });
