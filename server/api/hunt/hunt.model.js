'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var HuntSchema = new Schema({
  name: String,
  text: String,
  src: String,
  src_type: {type:String, default:'image'}, //video, link or image
  website: String,
  user_following : [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  posts: [{type: mongoose.Schema.Types.ObjectId, ref: 'Post'}],
  interactions : {
    total_upvote: { type: Number, default: 0 },
    total_downvote: { type: Number, default: 0 },
    total_posts: { type: Number, default: 0 },
    total_user_following: { type: Number, default: 0 }
  },

  createdBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  createdAt: {type: Date, default: Date.now },
  updatedAt: {type: Date, default: Date.now },
  lastActivity: {type: Date, default: Date.now },
  custom: {
    domain: {
      address: String,
      CNAME1: String,
      CNAME2: String,
      CNAME3: String
    }
  },
  active: Boolean

});

module.exports = mongoose.model('Hunt', HuntSchema);
