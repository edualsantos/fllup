'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/fllup'
  },
  SESSION_SECRET: "fllup-key",

  FACEBOOK_ID: '591946710942022',
  FACEBOOK_SECRET: '6eda745d36efa7c57075c3ef731b2ad9',

  TWITTER_ID: 'app-id',
  TWITTER_SECRET: 'secret',

  GOOGLE_ID: 'app-id',
  GOOGLE_SECRET: 'secret',

  seedDB: true
};
