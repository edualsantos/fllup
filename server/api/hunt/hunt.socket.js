/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Hunt = require('./hunt.model');

exports.register = function(socket) {
  Hunt.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Hunt.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('hunt:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('hunt:remove', doc);
}